import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Output('activePage') activePageEvent = new EventEmitter<number>();
  @Input() length: number;
  @Input() nbItemPerPage: number;
  currentPage: number;

  constructor() { }

  ngOnInit(): void {
  }

  get pageNumbers(): Array<number> {
    const pages = [];

    let nbPage = parseInt((this.length / this.nbItemPerPage).toString());
    const restPage = this.length % this.nbItemPerPage;

    if (restPage > 0) nbPage++;

    for(let i = 0; i < nbPage; i++) {
      pages.push(i);
    }

    return pages;
  }

  selectPage(e: MouseEvent, page: number) {
    e.preventDefault();
    if (page > 0 && page <= this.pageNumbers.length)
    this.activePageEvent.emit(page);
    this.currentPage = page;
  }
}
