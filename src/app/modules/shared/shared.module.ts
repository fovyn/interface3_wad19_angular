import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginationComponent } from './components/pagination/pagination.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EuropCurrencyPipe } from './pipes/europ-currency.pipe';
import { PaginationPipe } from './pipes/pagination.pipe';



@NgModule({
  declarations: [
    PaginationComponent,
    EuropCurrencyPipe,
    PaginationPipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [    
    PaginationComponent,
    EuropCurrencyPipe,
    PaginationPipe,
  ]
})
export class SharedModule { }
