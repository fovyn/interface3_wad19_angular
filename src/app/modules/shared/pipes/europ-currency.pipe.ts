import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'europCurrency'
})
export class EuropCurrencyPipe extends CurrencyPipe implements PipeTransform {

  transform(value: number): string {
    let  str = super.transform(value, 'EUR');
    str = str.replace("€", '');
    return str + ' €';
  }

}
