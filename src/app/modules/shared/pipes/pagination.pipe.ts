import { Pipe, PipeTransform } from '@angular/core';
import { SlicePipe } from '@angular/common';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(value: Array<any>, page: number, itemPerPage: number): Array<any> {
    const toDisplay = [];
    const startIndex = page * itemPerPage;
    const endIndex = startIndex + itemPerPage - 1;

    if (value.length > itemPerPage) {
      for(let i = startIndex; i <= endIndex; i++) {
        toDisplay.push(value[i]);
      }
    }

    return toDisplay;
  }

}
