import { TodoRoutingModule } from './todo-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TodoCreateComponent } from './components/todo-create/todo-create.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoComponent } from './components/todo/todo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { TodoEditComponent } from './components/todo-edit/todo-edit.component';



@NgModule({
  declarations: [
    TodoComponent,
    TodoListComponent,
    TodoCreateComponent,
    TodoEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    TodoRoutingModule,
  ],
  exports: []
})
export class TodoModule { }
