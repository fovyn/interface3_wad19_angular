import { TodoEditComponent } from './components/todo-edit/todo-edit.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoComponent } from './components/todo/todo.component';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { TodoCreateComponent } from './components/todo-create/todo-create.component';

const ROUTES: Routes = [
    { path: '', component: TodoComponent, children: [
        { path: 'list', component: TodoListComponent },
        { path: 'create', component: TodoCreateComponent },
        { path: 'edit/:id', component: TodoEditComponent }
    ] },
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
    ],
    exports: [
        RouterModule,
    ]
})
export class TodoRoutingModule {}