import { AbstractControl, FormControl, Validators, FormGroup } from '@angular/forms';

export const CREATE_FORM: {[key: string]: AbstractControl} = {
    title: new FormControl('', [Validators.required]),
    description: new FormControl('', []),
    dueDate: new FormControl('', [Validators.required]),
};