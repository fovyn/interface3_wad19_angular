import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { Todo } from 'src/app/models/todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  @Input() list: Array<Todo> = [];
  @Input() actions: Array<string> = [];
  @Output('delete') deleteEvent = new EventEmitter<Todo>();
  startIndex: number = 0;
  endIndex: number = 1;
  // currentPage = 0;

  constructor() { }

  ngOnInit(): void {
  }

  deleteAction(todo: Todo) {
    this.deleteEvent.emit(todo);
  }

  closeAction(todo: Todo) {
    todo.endDate = new Date();
  }

  displayPage(page: number) {
    this.startIndex = page * 2;
    this.endIndex = this.startIndex + 1;
    // this.currentPage = page;
  }
}
