import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Todo } from 'src/app/models/todo.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CREATE_FORM } from '../../forms/todo.form';

@Component({
  selector: 'app-todo-create',
  templateUrl: './todo-create.component.html',
  styleUrls: ['./todo-create.component.css']
})
export class TodoCreateComponent implements OnInit {
  @Output('create') createEvent = new EventEmitter<Todo>(); 

  form: FormGroup;
  endDate: string;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group(CREATE_FORM);
    const array = this.formBuilder.array([]);
    array.push(this.form);
  }

  createAction(): void {
    if (this.form.valid) {
      const newTodo = new Todo();
      Object.assign(newTodo, this.form.value);
      this.createEvent.emit(newTodo);
      console.log(newTodo);
    }
  }
}
