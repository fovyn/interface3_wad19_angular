import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo.model';
import * as M from 'materialize-css/dist/js/materialize.js';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  private _todoes: Array<Todo> = new Array<Todo>();

  get todoes(): Array<Todo> {
    return this._todoes.filter(t => !t.isDelete);
  }

  get removedTodoes(): Array<Todo> {
    return this._todoes.filter(t => t.isDelete);
  }

  constructor() { }

  ngOnInit(): void {
    M.AutoInit();
  }

  /**
   * Method need to add the created todo to the displayed list
   * @param {Todo} todo
   */
  addToList(todo: Todo): void {
    const isFound = this.searchForDuplicate(todo);

    if (isFound == -1) {
      this._todoes.push(todo);
    }
    if (isFound != -1 && this._todoes[isFound].isDelete) {
      this._todoes[isFound].isDelete = false;
    }
  }

  removeFromList(todo: Todo) {
    const isFound = this.searchForDuplicate(todo);
    if (isFound != -1) {
      // this._todoes.splice(isFound, 1);
      this._todoes[isFound].isDelete = true;
    }
  }

  private searchForDuplicate(todo: Todo): number {
    const found = this._todoes.findIndex((t: Todo) => t.title === todo.title);

    return found;
  }
}
