import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { PersonComponent } from './components/person/person.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';


const routes: Routes = [
  { path: '', component: PersonComponent, children: [
    { path: 'create', component: RegisterComponent },
    { path: 'list', component: ListComponent }
  ]}
];

@NgModule({
  declarations: [PersonComponent, RegisterComponent, ListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class PersonModule { }
