import { PersonService } from './../../services/person.service';
import { CREATE_FORM } from './../../../todo/forms/todo.form';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PERSON_CREATE_FORM } from '../../forms/person.form';
import { Router } from '@angular/router';

@Component({
  selector: 'person-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  createForm: FormGroup;

  constructor(private builder: FormBuilder, private personService: PersonService, private router: Router) { }

  ngOnInit(): void {
    this.createForm = this.builder.group(PERSON_CREATE_FORM);
  }

  addPhoneNumber() {
    const phoneNumberArray: FormArray = this.createForm.get('phoneNumbers') as FormArray;

    phoneNumberArray.push(new FormGroup(CREATE_FORM));
  }

  get PhoneNumbersControls() {
    return (this.createForm.get('phoneNumbers') as FormArray).controls;
  }

  onSubmit() {
    if (this.createForm.valid) {
      this.personService.add(this.createForm.value);
    }
  }

}
