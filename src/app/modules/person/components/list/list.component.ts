import { PersonService } from './../../services/person.service';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {map, flatMap} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'person-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  persons$: Observable<any>;
  constructor(private personService: PersonService, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.persons$ = this.personService.Observable;
    this.persons$.subscribe(data => console.log(data));
    this.httpClient.get<any>(`https://pokeapi.co/api/v2/pokedex`).pipe(map(data => data.results)).subscribe(data => console.log(data));
  }

}
