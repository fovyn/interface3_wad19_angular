import { AbstractControl, FormControl, FormArray } from '@angular/forms';
export const PERSON_CREATE_FORM: {[key: string]: AbstractControl} = {
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    phoneNumbers: new FormArray([])
};