import { Injectable } from '@angular/core';
import { BehaviorSubject, PartialObserver, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private persons$: BehaviorSubject<any> = new BehaviorSubject([{firstName: "BLOP"}]);

  constructor() { }

  add(obj: any) {
    this.persons$.next([...this.persons$.value, obj]);
  }

  subscribe(next: PartialObserver<any>) {
    this.persons$.subscribe(next);
  }

  get Observable(): Observable<any> {
    console.log(this.persons$);
    return this.persons$.asObservable();
  }
}
