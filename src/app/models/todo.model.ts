export class Todo {
    title: string;
    description: string;
    dueDate: Date;
    endDate: Date;
    isDelete: boolean = false;
}