import { TodoModule } from './modules/todo/todo.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './components/app/app.component';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  { path: 'todo', loadChildren: () => import('./modules/todo/todo.module').then(m => m.TodoModule)},
  { path: 'person', loadChildren: () => import('./modules/person/person.module').then(m => m.PersonModule) }
]

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
